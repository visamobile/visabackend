<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:45
 */

namespace app\utils;


class StringsHelper
{
    public function __construct()
    {
    }

    /**
     * Нормолизует номер до 4-ех симовлов
     *
     * @param $zoneNumber
     *
     * @return string
     */
    public static function normolizeParkingZoneNumber($zoneNumber)
    {
        return sprintf('%04d', $zoneNumber);
    }

    /**
     * Нормолизует то что присылает пользователь отрезает все что после пробела
     *
     * @param $zoneNumber
     *
     * @return string
     */
    public static function normolizeParkingZoneNumberFromUser($zoneNumberRaw)
    {
        list($zoneNumber) = explode(" ", $zoneNumberRaw);

        return trim($zoneNumber);
    }

    /**
     * @param string|int $phoneRaw
     *
     * @return string
     */
    public function normolizePhone($phoneRaw)
    {
        $phoneCleanedRaw = preg_replace('{[^\\d]}', '', $phoneRaw);
        $changeTo = '7';
        return substr_replace($phoneCleanedRaw, $changeTo, 0, 1);
    }
}