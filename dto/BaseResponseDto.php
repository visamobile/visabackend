<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:10
 */

namespace app\dto;

/**
 * Class BaseResponseDto
 *
 * @SWG\Definition(definition="BaseResponseDto")
 * @package app\modules\api\dto
 */
class BaseResponseDto
{
    /**
     * Статус ответа хорошо
     * @SWG\Property(
     *     description="Статус ответа хорошо"
     * )
     * @var string
     */
    public $status;

    /**
     * BaseResponseDto constructor.
     *
     * @param string $status
     */
    public function __construct($status = 'ok')
    {
        $this->status = $status;
    }

    /**
     * @param string $status
     *
     * @return BaseResponseDto
     */
    public static function create($status = 'ok')
    {
        return new static($status);
    }
}