<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 16:35
 */

namespace app\dto;


/**
 * Class ResponseErrorDto
 *
 * @SWG\Definition(definition="ResponseErrorDto")
 * @package app\modules\api\dto
 */
class ResponseErrorDto
{
    /**
     * Статус ответа ошибка
     * @SWG\Property(
     *     description="Статус ответа ошибка"
     * )
     * @var string
     */
    public $status = 'error';
    /**
     * @SWG\Property(
     *     @SWG\Schema(ref="#/definitions/ErrorDto")
     * )
     * @var ErrorDto
     */
    public $error;

    public function __construct(ErrorDto $error)
    {
        $this->error = $error;
    }

    /**
     * @param int    $code
     * @param string $message
     * @param array  $properties
     *
     * @return ResponseErrorDto
     */
    public static function create($code = 0, $message = '', array $properties = [])
    {
        return new static(ErrorDto::create($code, $message, $properties));
    }
}