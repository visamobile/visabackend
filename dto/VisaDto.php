<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 16.09.17
 * Time: 14:58
 */

namespace app\dto;


class VisaDto
{
    /* @var  integer*/
    public $user_id;
    /* @var integer */
    public $country_id;
    /* @var integer */
    public $type;

    public function __construct($user_id, $country_id, $type)
    {
        $this->user_id = $user_id;
        $this->country_id = $country_id;
        $this->type = $type;
    }

}