<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 19.09.17
 * Time: 14:19
 */

namespace app\dto;


class JwtDto
{
    const FORMAT_DATE_FOR_DB = 'Y-m-d H:i:s';

    /* @var integer */
    public $userId;
    /* @var integer */
    public $phoneNumber;
    /* @var dataTime */
    public $createdAt;

    public function __construct($userId, $phoneNumber)
    {
        $this->userId = $userId;
        $this->phoneNumber = $phoneNumber;
        $this->createdAt =   date(self::FORMAT_DATE_FOR_DB);//\DateTime::createFromFormat(self::FORMAT_DATE_FOR_DB);;
    }

}