<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 16.09.17
 * Time: 13:15
 */

namespace app\dto;


class VisaTasksDto
{
    /* @var integer */
    public $id;
    /* @var integer */
    public $coutry_id;
    /* @var integer */
    public $type;
    /* @var string */
    public $value;
    /* @var integer */
    public $numberInAnket;
    /* @var json */
    public $rules;
    /* @var integer */
    public $version;

    public function __construct($id, $coutry_id, $type, $value, $numberInAnket, $rules, $version)
    {
        $this->id = $id;
        $this->coutry_id = $coutry_id;
        $this->type = $type;
        $this->value = $value;
        $this->number_in_anket = $numberInAnket;
        $this->rules = $rules;
        $this->version = $version;
    }

}