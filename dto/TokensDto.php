<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 20:28
 */

namespace app\dto;


class TokensDto
{
    use LoadFromArray;

    public $refreshToken;
    public $accessToken;

    public function __construct($refreshToken = null, $accessToken = null)
    {
        $this->refreshToken = $refreshToken;
        $this->accessToken = $accessToken;
    }

}