<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:07
 */

namespace app\dto;


trait LoadFromArray
{
    public static function loadFromArray(array $data)
    {
        $self = new static();
        foreach ($data as $propertyName => $propertyValue) {
            if (!property_exists($self, $propertyName)) {
                continue;
            }
            $self->$propertyName = $propertyValue;
        }

        return $self;
    }
}