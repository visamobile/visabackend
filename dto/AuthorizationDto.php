<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:08
 */

namespace app\dto;


use app\dto\LoadFromArray;

/**
 * Class AuthorizationDto
 *
 * @SWG\Definition(definition="AuthorizationDto")
 * @package app\modules\api\dto\auth
 */
class AuthorizationDto
{
    use LoadFromArray;

    /**
     * Телефонный номер
     * @SWG\Property(
     *     description="Телефонный номер"
     * )
     * @var string
     */
    public $phone;
    /**
     * Пин-код
     * @SWG\Property(
     *     description="Пин-код"
     * )
     * @var string
     */
    public $pin;
}