<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 14.09.17
 * Time: 12:14
 */

namespace app\dto;


class UserAppDto
{
    use LoadFromArray;

    /** @var integer */
    public $phone_number;
    /** @var  string */
    public $country;
    /** @var  string */
    public $language;
    /** @var  string */
    public $email;

    public function __construct($phone_number, $country, $language, $email = null)
    {
        $this->phone_number = $phone_number;
        $this->country = $country;
        $this->language = $language;
        $this->email = $email;
    }
}