<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:09
 */

namespace app\dto;


use app\dto\BaseResponseDto;

/**
 * Class ResponseGetTokenDto
 *
 * @SWG\Definition(definition="ResponseGetTokenDto")
 * @package app\modules\api\dto\auth
 */
class ResponseGetTokenDto extends BaseResponseDto
{
    /**
     * Токен для мобильного приложения
     * @SWG\Property(
     *     description="Токен для мобильного приложения"
     * )
     * @var string
     */
    public $token;

    /**
     * ResponseGetTokenDto constructor.
     *
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
        parent::__construct();
    }

    /**
     * @param string $token
     *
     * @return ResponseGetTokenDto
     */
    public static function create($token)
    {
        return new static($token);
    }
}