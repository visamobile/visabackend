<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:21
 */

namespace app\dto;

use app\dto\LoadFromArray;

/**
 * Class SendSmsDto
 *
 * @SWG\Definition(definition="SendSmsDto")
 * @package app\modules\api\dto\auth
 */
class SendSmsDto
{
    use LoadFromArray;

    /**
     * Телефонный номер
     * @SWG\Property(
     *     description="Телефонный номер"
     * )
     * @var string
     */
    public $phone;
}