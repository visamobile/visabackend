<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 16:25
 */

namespace app\exceptions;

/**
 * interface DisplayWebException
 */
interface DisplayWebException {
    /**
     * @return array
     */
    public function getProperties();
}