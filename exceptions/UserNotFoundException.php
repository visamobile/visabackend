<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:02
 */

namespace app\exceptions;

use app\exceptions\DisplayWebException;
use Exception;

class UserNotFoundException extends \Exception implements DisplayWebException
{
    protected $code = 1304;
    protected $message = 'К сожалению, мы не смогли найти такого пользователя :(';

    public function __construct($message = '', $code = 0, Exception $previous = null)
    {
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [];
    }
}