<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 18:28
 */

namespace app\exceptions;



use app\exceptions\DisplayWebException;
use Exception;

class SmsSessionsToAuthNotFoundException extends \Exception implements DisplayWebException
{
    protected $code = 1204;
    protected $message = 'Не удалось найти SmsSessionToAuth.';

    public function __construct($message = '', $code = 0, Exception $previous = null)
    {
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [];
    }
}