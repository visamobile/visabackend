<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 16:16
 */

namespace app\exceptions;

use app\exceptions\DisplayWebException;


class TokenEmptyException extends \Exception implements DisplayWebException
{
    protected $code = 805;

    protected $message = 'Токен пуcтой.';

    public function __construct(array $errors = [], $code = 0, \Exception $previous = null)
    {
        $this->properties = $errors;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [];
    }
}