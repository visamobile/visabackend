<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 12:50
 */

namespace exceptions;

use Exception;
use app\exceptions\DisplayWebException;

class UserWithThisPhoneNumberExistException extends \Exception implements DisplayWebException
{
    protected $code = 801;
    protected $message = 'Пользователь с таким номером телефона уже существует.';
    protected $properties = [];

    public function __construct(array $properties = [], $code = 0, Exception $previous = null)
    {
        $this->properties = $properties;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

}