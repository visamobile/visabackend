<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 16:23
 */

namespace app\exceptions;

use app\exceptions\DisplayWebException;


/**
 * Class YouNotLogedException
 */
class YouNotLogedException extends \Exception implements DisplayWebException
{
    protected $code = 1003;

    protected $message = 'У вас отсутсвует авторизационный токен, "Authorization: Bearer <token>".';

    /**
     * @return array
     */
    public function getProperties()
    {
        return [];
    }
}