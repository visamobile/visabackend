<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 14:01
 */

namespace exceptions;

use \Exception;
use app\exceptions\DisplayWebException;

class ContryNotFoundInDB extends Exception implements DisplayWebException
{
    protected $code = 804;
    protected $message = 'Не удалось найти страну в базе.';
    protected $properties = [];

    public function __construct(array $properties = [], $code = 0, Exception $previous = null)
    {
        $this->properties = $properties;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}