<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 19:58
 */

namespace exceptions;

use \Exception;
use app\exceptions\DisplayWebException;

class RefreshTokenNotValid extends \Exception implements DisplayWebException
{
    protected $code = 806;
    protected $message = 'Refresh token  не валидный.';
    protected $properties = [];

    public function __construct(array $properties = [], $code = 0, Exception $previous = null)
    {
        $this->properties = $properties;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}