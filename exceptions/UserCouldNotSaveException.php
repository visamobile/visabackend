<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 13:24
 */

namespace exceptions;

use \Exception;
use app\exceptions\DisplayWebException;

class UserCouldNotSaveException extends \Exception implements DisplayWebException
{
    protected $code = 802;
    protected $message = 'Не удалось сохранить нового пользователя в базу.';
    protected $properties = [];

    public function __construct(array $properties = [], $code = 0, Exception $previous = null)
    {
        $this->properties = $properties;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}