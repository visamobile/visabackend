<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 16:30
 */

namespace app\exceptions;

use app\exceptions\DisplayWebException;
use app\dto\ResponseErrorDto;
use yii\web\Response;

/**
 * Class ExceptionsHandler
 */
class ExceptionsHandler
{
    public function __construct()
    {
    }

    /**
     * Метод отлавливает \Exeption и обрабатывает их,
     * если \Exeption имплементирует TelegramException,
     * тогда выводит его код и сообщение ошибки
     *
     * @param          $id
     * @param          $params
     * @param callable $function
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function execute($id, $params, callable $function)
    {
        try {
            return $function($id, $params);

        } catch (\Exception $e) {
            $code = $e->getCode();

            switch (true) {
                case $e instanceof DisplayWebException:
                    $response             = \Yii::$app->response;
                    $response->format     = Response::FORMAT_JSON;
                    $response->data       = ResponseErrorDto::create($e->getCode(), $e->getMessage(),
                        $e->getProperties());
                    $response->statusCode = 400;
                    \Yii::$app->end($response->statusCode, $response);

                    return;
                    break;
                default:
                    $this->logException($e);

                    $message = 'Internal server error';
            }

            $this->sendErrorResponse($code, $message);
        }

        return null;
    }

    public function executeFn(callable $function)
    {
        try {
            return $function();

        } catch (\Exception $e) {
            $code = $e->getCode();

            switch (true) {
                case $e instanceof DisplayWebException:
                    $response             = \Yii::$app->response;
                    $response->format     = Response::FORMAT_JSON;
                    $response->data       = ResponseErrorDto::create($e->getCode(), $e->getMessage(),
                        $e->getProperties());
                    $response->statusCode = 400;
                    \Yii::$app->end($response->statusCode, $response);

                    return;
                    break;
                default:
                    $this->logException($e);

                    $message = 'Internal server error';
            }

            $this->sendErrorResponse($code, $message);

        }

        return null;
    }

    /**
     * @param \Exception $e
     */
    protected function logException(\Exception $e)
    {
        \Yii::error($e);
    }

    /**
     * Sends error response
     *
     * @param int    $code
     * @param string $message
     */
    protected function sendErrorResponse($code, $message)
    {
        $response         = \Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->setStatusCode($code, $message);

        \Yii::$app->end(0, $response);
    }
}