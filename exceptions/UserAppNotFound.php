<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 13:06
 */

namespace exceptions;

use Exception;
use app\exceptions\DisplayWebException;

class UserAppNotFound extends Exception implements DisplayWebException
{
    protected $code = 800;
    protected $message = 'Пользователь не найден в базе.';
    protected $properties = [];

    public function __construct(array $properties = [], $code = 0, Exception $previous = null)
    {
        $this->properties = $properties;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}