<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 14.09.17
 * Time: 11:36
 */

namespace app\repositories;

use app\models\VisaTasks;

class VisaTasksRepository
{
    public function __construct()
    {
    }

    public function save(VisaTasks $visaTasks)
    {
        $visaTasks->save();
    }

    public function getById($id)
    {
        return VisaTasks::find()->where(['id' => $id])->limit(1)->one();
    }

    public function getByNumberInAnketa($numberInAnket)
    {
        return VisaTasks::find()->where(['number_in_anket' => $numberInAnket])->max(['version']);
    }
}