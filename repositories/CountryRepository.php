<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 11.09.17
 * Time: 20:44
 */

namespace app\repositories;

use app\models\Country;

class CountryRepository
{
    public function __construct()
    {
    }

    public function save(Country $country)
    {
        $country->save();
    }

    public function getById($id)
    {
        return Country::find()->where(['id' => $id])->limit(1)->one();
    }

    public function getByName($name)
    {
        return Country::find()->where(['name' => $name])->limit(1)->one();
    }
}