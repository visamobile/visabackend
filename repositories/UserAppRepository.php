<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 07.09.17
 * Time: 17:22
 */

namespace app\repositories;

use app\models\UserApp;
use exceptions\UserCouldNotSaveException;

class UserAppRepository
{
    public  function __construct()
    {
    }

    public function save(UserApp $userApp)
    {
        $userApp->save();
        if (count($userApp->errors) > 0) {
            /** @noinspection ThrowRawExceptionInspection */
            throw new UserCouldNotSaveException($userApp->errors);
        }
    }

    public function getByPhoneNumber($phoneNumber)
    {
        return UserApp::find()->where(['phone_number' => $phoneNumber,
            'status' => UserApp::STATUS_ACTIVE,
            ])->limit(1)->one();
    }

    public  function  getAll()
    {
        return UserApp::find()->where(['status'=> UserApp::STATUS_ACTIVE])->orderBy('date')->all();
    }

    public function getById($userAppId)
    {
        return UserApp::find()->where(['id' => $userAppId,
                                        'status' => UserApp::STATUS_ACTIVE,
            ])->limit(1)->one();
    }

    public function getByEmail($email)
    {
        return UserApp::find()->where(['email' => $email,
                                        'status' => UserApp::STATUS_ACTIVE,
        ])->limit(1)->one();
    }
}