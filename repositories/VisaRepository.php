<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 11.09.17
 * Time: 21:14
 */

namespace app\repositories;

use app\models\Visa;

class VisaRepository
{
    public function __construct()
    {
    }

    public function save(Visa $visa)
    {
        $visa->save();
    }

    public function getByUserId($user_id)
    {
        return Visa::find()->where(['user_id' => $user_id])->limit(1)->one();
    }

}