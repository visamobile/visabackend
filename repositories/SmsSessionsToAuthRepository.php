<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 18:49
 */

namespace app\repositories;

use app\exceptions\SmsSessionsToAuthCouldNotSaveException;
use app\models\SmsSessionToAuth;

class SmsSessionsToAuthRepository
{
    const TEMPLATE_DATE_INTERVAL = 'P0Y0M0DT0H%dM0S';
    const MINUS_MINUTS           = 10;

    public function __construct()
    {
    }

    /**
     * @param SmsSessionToAuth $smsSessionToAuth
     *
     * @throws SmsSessionsToAuthCouldNotSaveException
     */
    public function save(SmsSessionToAuth $smsSessionToAuth)
    {
        $smsSessionToAuth->save();
        if (count($smsSessionToAuth->errors) > 0) {
            throw new SmsSessionsToAuthCouldNotSaveException($smsSessionToAuth->errors);
        }
    }

    /**
     * @param $phone
     * @param $pin
     *
     * @return null|SmsSessionToAuth
     */
    public function getSessionActiveByPhoneAndPin($phone, $pin)
    {
        // костыль для прохождения ревью в App Store & Google Play

        $pinTimeToLiveInMinutes = self::MINUS_MINUTS;
        if ($phone == '79653289536') {
            $pinTimeToLiveInMinutes = 30000;
        }
        $timeMinusString = sprintf(self::TEMPLATE_DATE_INTERVAL, $pinTimeToLiveInMinutes);
        $timeMinus10Min  = (new \DateTime())->sub(new \DateInterval($timeMinusString));

        return SmsSessionToAuth::find()->where([
            'phone'  => $phone,
            'pin'    => (int)$pin,
            'status' => SmsSessionToAuth::STATUS_ACTIVE,
        ])
            ->andWhere('created_at >= :created_at', [':created_at' => $timeMinus10Min->format('c')])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(1)->one();
    }

    /**
     * @param $phone
     *
     * @return null|SmsSessionToAuth
     */
    public function getSessionActiveByPhone($phone)
    {
        $timeMinusString = sprintf(self::TEMPLATE_DATE_INTERVAL, self::MINUS_MINUTS);
        $timeMinus10Min  = (new \DateTime())->sub(new \DateInterval($timeMinusString));

        return SmsSessionToAuth::find()->where([
            'phone'  => $phone,
            'status' => SmsSessionToAuth::STATUS_ACTIVE,
        ])
            ->andWhere('created_at >= :created_at', [':created_at' => $timeMinus10Min->format('c')])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(1)->one();
    }
}