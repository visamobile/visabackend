<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 19.09.17
 * Time: 11:44
 */

namespace app\repositories;

use app\models\RefreshToken;
use exceptions\RefreshTokenCouldNotSave;

class RefreshTokenRepository
{
    public function __construct()
    {
    }

    public function save(RefreshToken $refreshToken)
    {
        $refreshToken->save();
        if (count($refreshToken->errors) > 0) {
            /** @noinspection ThrowRawExceptionInspection */
            throw new RefreshTokenCouldNotSave($refreshToken->errors);
        }
    }

    /**
     * @param $value
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getByValue($value)
    {
        return RefreshToken::find()->where(['value' => $value,
                                        'status' => RefreshToken::STATUS_ACTIVE])->limit(1)->one();
    }


}