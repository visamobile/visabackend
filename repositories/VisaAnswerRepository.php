<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 26.09.17
 * Time: 12:56
 */

namespace app\repositories;


use app\models\VisaAnswer;

class VisaAnswerRepository
{
    public function __construct()
    {
    }

    public function save(VisaAnswer $visaAnswer)
    {
        $visaAnswer->save();
    }

}