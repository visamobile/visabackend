<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 18:33
 */

/**
 * Интерфейс работы с СМС сервисом
 * @package app\interfaces
 */
interface SmsServiceInterface
{
    /**
     * Отправка СМС
     * @return string
     */
    public function send();

    /**
     * Установка телефона
     * @param string $phone
     * @return string
     */
    public function setPhone($phone);

    /**
     * Установка телефона сообщения
     * @param string $message
     * @return string
     */
    public function setMessage($message);
}