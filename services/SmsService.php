<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 18:34
 */

namespace app\services;


use app\interfaces\SmsServiceInterface;

/**
 * Компонента смс сервиса
 * @package app\components
 */
class SmsService implements SmsServiceInterface
{
    /** @var string $phone Телефон */
    private $phone;

    /** @var string $message Сообщение */
    private $message;

    /** @var string */
    private $userName;
    /** @var string */
    private $password;
    /** @var string */
    private $urlToSmsService;

    /**
     * SmsServiceInterface constructor.
     *
     * @param $userName
     * @param $password
     * @param $urlToSmsService
     */
    public function __construct($userName, $password, $urlToSmsService)
    {
        $this->userName = $userName;
        $this->password = $password;
        $this->urlToSmsService = $urlToSmsService;
    }

    /**
     * Установка телефона
     *
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $phone       = preg_replace('/\D/', '', $phone);
        $phone[0]    = '7';
        $this->phone = $phone;

        return $this;
    }

    /**
     * Установка сообщения
     *
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Билдинг запроса на отправку смс
     *
     * @return string
     */
    private function buildSmsXml()
    {
        return '<?xml version="1.0" encoding="UTF-8"?>
            <SMS>
            <operations>
                <operation>SEND</operation>
            </operations>
            <authentification>
                <username>' . $this->userName . '</username>
                <password>' . $this->password . '</password>
            </authentification>
            <message>
                <sender>Parkomatica</sender>
                <text>' . $this->message . '</text>
            </message>
            <numbers>
                <number messageID="msg11">' . $this->phone . '</number>
            </numbers>
            </SMS>';
    }

    /**
     * Построение и отправка CURL запроса
     *
     * @return mixed
     */
    private function curlSend()
    {
        $curl    = curl_init();
        $options = [
            CURLOPT_URL            => $this->urlToSmsService,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_POST           => true,
            CURLOPT_HEADER         => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 15,
            CURLOPT_TIMEOUT        => 100,
            CURLOPT_POSTFIELDS     => ['XML' => $this->buildSmsXml()],
        ];
        curl_setopt_array($curl, $options);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    /**
     * Отправка СМС
     *
     * @throws \Exception
     * @return mixed
     */
    public function send()
    {
        if (empty($this->phone) || empty($this->message)) {
            throw new \Exception('Ошибка параметров СМС');
        }

        return $this->curlSend();
    }
}