<?php

use yii\db\Migration;

class m170906_083210_create_table_country extends Migration
{
    const TABLE_NAME = 'country';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'last_version' => $this->bigInteger(),
            ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
