<?php

use yii\db\Migration;

class m170906_101037_create_table_visa_tasks extends Migration
{
    const TABLE_NAME = 'visa_tasks';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(),
            'type' => $this->smallInteger(),
            'value' => $this->text(),
            'number_in_anket' => $this->integer(),
            'rules' => 'json',
            'version' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-'.self::TABLE_NAME.'-country_id',
            self::TABLE_NAME,
            'country_id',
            'country',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
