<?php

use yii\db\Migration;

class m170906_163745_create_table_visa extends Migration
{
    const TABLE_NAME = 'visa';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status' => $this->string(),
            'country_id' => $this->integer(),
            'type' => $this->integer(),
            'created_at' => $this->dateTime(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
