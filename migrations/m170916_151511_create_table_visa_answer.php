<?php

use yii\db\Migration;

class m170916_151511_create_table_visa_answer extends Migration
{
    const TABLE_NAME = 'visa_answer';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'value' => $this->text(),
            'task_id' => $this->bigInteger(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
