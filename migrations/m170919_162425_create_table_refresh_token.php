<?php

use yii\db\Migration;

class m170919_162425_create_table_refresh_token extends Migration
{
    const TABLE_NAME = 'refresh_token';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->bigInteger(),
            'status' => $this->string(),
            'value' => $this->string(),
            'created_at' => $this->dateTime()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
