<?php

use yii\db\Migration;

class m170905_181857_create_table_user extends Migration
{
    const TABLE_NAME = 'users';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'status' => $this->string(),
            'phone_number' => $this->bigInteger(),
            'country' => $this->string(),
            'language' => $this->string(),
            'email' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
