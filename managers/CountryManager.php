<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 15.09.17
 * Time: 12:45
 */

namespace app\manager;

use app\models\Country;
use app\repositories\CountryRepository;
use exceptions\ContryNotFoundInDB;

class CountryManager
{
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function createdCountry($name)
    {
        $country = new Country([
            'name' => $name,
            'last_version' => 0,
        ]);

        $this->countryRepository->save($country);
        return $country;
    }

    public function updateTasksVersionOfTheCountryById($id, $version)
    {
        $country = $this->countryRepository->getById($id);

        if ($country === null) {
            throw new ContryNotFoundInDB();
        }

        $country->last_version = $version ;

        $country->save();

        return $country;
    }

    public function updateTasksVersionOfTheCountryByName($name, $version)
    {
        $country = $this->countryRepository->getByName($name);

        if ($country === null) {
            throw new ContryNotFoundInDB();
        }

        $country->last_version = $version;

        $country->save();

        return $country;
    }
}