<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 26.09.17
 * Time: 13:00
 */

namespace app\managers;


use app\models\VisaAnswer;
use app\repositories\VisaAnswerRepository;

class VisaAnswerManager
{
    private $visaAnswerReposytory;

    public function __construct(VisaAnswerRepository $visaAnswerRepository)
    {
        $this->visaAnswerReposytory = $visaAnswerRepository;
    }

    public function createVisaAnswer($value, $taskId)
    {
        $visaAnswer = new VisaAnswer([
            'value' => $value,
            'task_id' => $taskId,
        ]);

        $this->visaAnswerReposytory->save($visaAnswer);

        return $visaAnswer;
    }
}