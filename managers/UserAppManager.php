<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 09.09.17
 * Time: 20:03
 */

namespace app\managers;

use app\repositories\UserAppRepository;
use app\models\UserApp;
use app\dto\UserAppDto;
use exceptions\UserWithThisPhoneNumberExistException;
use app\exceptions\UserNotFoundException;
use app\utils\StringsHelper;

class UserAppManager
{
    const FORMAT_DATE_FOR_DB = 'Y-m-d H:i:s';

    private $userAppRerository;

    private $stringsHelper;

    public function __construct(UserAppRepository $userAppRepository, StringsHelper $stringsHelper)
    {
        $this->userAppRerository = $userAppRepository;
        $this->stringsHelper = $stringsHelper;
    }

    /**
     * @param UserAppDto $userAppDto
     * @return UserApp
     */
    public function createNewUserApp(UserAppDto $userAppDto)
    {
        if ($this->IsNotExistUserForThisPhoneNumber($userAppDto->phone_number)) {
            $userApp = new UserApp([
                'phone_number' => $userAppDto->phone_number,
                'status' => UserApp::STATUS_ACTIVE,
                'country' => $userAppDto->country,
                'language' => $userAppDto->language,
                'email' => $userAppDto->email,
                'created_at' => date(self::FORMAT_DATE_FOR_DB),
            ]);
            $this->userAppRerository->save($userApp);
            return $userApp;
        }
    }

    /**
     * @param $userApp
     */
    public function deleteUserApp($userApp)
    {
        $userApp->status = UserApp::STATUS_DELETED;
        $this->userAppRerository->save($userApp);
    }

    /**
     * @param $userAppId
     * @throws \Exception
     */
    public function deleteUserAppById($userAppId)
    {
        $userApp = $this->userAppRerository->getById($userAppId);
        if ($userApp === null) {
            throw new UserNotFoundException();
        }

        $this->deleteUserApp($userApp);
    }

    /**
     * @param $phoneNumber
     * @throws \Exception
     */
    public function deleteUserAppByPhoneNumber($phoneNumber)
    {
        $userApp = $this->userAppRerository->getByPhoneNumber($phoneNumber);
        if ($userApp === null) {
            throw new UserNotFoundException();
        }

        $this->deleteUserApp($userApp);
    }

    /**
     * @param $phoneNumber
     * @return bool
     * @throws \Exception
     */
    public function IsNotExistUserForThisPhoneNumber($phoneNumber)
    {
        $userApp = $this->userAppRerository->getByPhoneNumber($phoneNumber);
        if ($userApp === null) {
            return true;
        }
        else {
            throw new UserWithThisPhoneNumberExistException();
        }
    }

    public function getUserAppById($id)
    {
        $userApp = $this->userAppRerository->getById($id);
        return $userApp;
    }

    public function getUserByPhone($phoneRaw)
    {
        $phone = $this->stringsHelper->normolizePhone($phoneRaw);
        $user  = $this->userAppRerository->getByPhoneNumber($phone);
        if ($user === null) {
            throw new UserNotFoundException();
        }

        return $user;
    }
}