<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 15.09.17
 * Time: 16:14
 */

namespace app\manager;

use app\repositories\VisaTasksRepository;
use app\models\VisaTasks;
use app\dto\VisaTasksDto;

class VisaTasksManager
{
    private $visaTasksRepository;

    public function __construct(VisaTasksRepository $visaTasksRepository)
    {
        $this->visaTasksRepository = $visaTasksRepository;
    }

    public function createVisaTask(VisaTasksDto $visaTasksDto)
    {
        $visaTask = new VisaTasks([
            '$coutry_id' => $visaTasksDto->coutry_id,
            'type' => $visaTasksDto->type,
            'value' => $visaTasksDto->value,
            'number_in_anket' => $visaTasksDto->numberInAnket,
            'rules' => $visaTasksDto->rules,
            'version' => $visaTasksDto->version,
        ]);

        $this->visaTasksRepository->save($visaTask);
        return $visaTask;
    }

    private function updateVisaTask(VisaTasks $visaTask, VisaTasksDto $visaTasksDto)
    {
        if ($visaTasksDto->coutry_id != null) {
            $visaTask->country_id = $visaTasksDto->coutry_id;
        }

        if ($visaTasksDto->type != null) {
            $visaTask->type = $visaTasksDto->type;
        }

        if ($visaTasksDto->value != null) {
            $visaTask->value = $visaTasksDto->value;
        }

        if ($visaTasksDto->numberInAnket != null) {
            $visaTask->number_in_anket = $visaTasksDto->numberInAnket;
        }

        if ($visaTasksDto->rules != null) {
            $visaTask->rules = $visaTasksDto->rules;
        }

        $this->visaTasksRepository->save($visaTask);
    }

    public function updateVisaTaskById($id, VisaTasksDto $visaTasksDto)
    {
        $visaTask = $this->visaTasksRepository->getById($id);

        if ($visaTask === null) {
            throw new \Exception('Task not found.', 608);
        }

        $this->updateVisaTask($visaTask, $visaTasksDto);

        return $visaTask;
    }

    public function updateVisaTaskByNumberInAnketa($numberInAnketa, VisaTasksDto $visaTasksDto)
    {
        $visaTask = $this->visaTasksRepository->getByNumberInAnketa($numberInAnketa);

        if ($visaTask === null) {
            throw new \Exception('Task not found.', 608);
        }

        $this->updateVisaTask($visaTask, $visaTasksDto);

    }
}