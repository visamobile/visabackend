<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 16.09.17
 * Time: 14:53
 */

namespace app\manager;

use app\dto\VisaDto;
use app\models\Visa;
use app\repositories\VisaRepository;

class VisaManager
{
    private $visaRepository;

    public function __construct(VisaRepository $visaRepository)
    {
        $this->visaRepository = $visaRepository;
    }

    public function createdVisa(VisaDto $visaDto)
    {
        $visa = new Visa([
            'user_id' => $visaDto->user_id,
            'status' => Visa::STATUS_ACTIVE,
            'country_id' => $visaDto->country_id,
            'type' => $visaDto->type,
        ]);

        $this->visaRepository->save($visa);

        return $visa;
    }

    private function deleteVisa(Visa $visa)
    {
        $visa->status = Visa::STATUS_DELETED;
        $this->visaRepository->save($visa);
    }

    public function deleteVisaByUserId($user_id)
    {
        $visa = $this->visaRepository->getByUserId($user_id);

        $this->deleteVisa($visa);

    }
}