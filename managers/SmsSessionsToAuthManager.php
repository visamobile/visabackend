<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:24
 */

namespace app\managers;

use app\exceptions\SmsSessionsToAuthNotFoundException;
use app\models\SmsSessionToAuth;
use app\repositories\SmsSessionsToAuthRepository;
use app\services\SmsService;
use app\utils\StringsHelper;

class SmsSessionsToAuthManager
{
    /** @var SmsSessionsToAuthRepository */
    private $smsSessionsToAuthRepository;
    /** @var SmsService */
    private $smsService;
    /** @var UserManager */
    private $userAppManager;
    /** @var StringsHelper */
    private $stringsHelper;

    public function __construct(
        SmsSessionsToAuthRepository $smsSessionsToAuthRepository,
        SmsService $smsService,
        UserAppManager $userAppManager,
        StringsHelper $stringsHelper
    ) {
        $this->smsSessionsToAuthRepository = $smsSessionsToAuthRepository;
        $this->smsService                  = $smsService;
        $this->userAppManager              = $userAppManager;
        $this->stringsHelper               = $stringsHelper;
    }

    /**
     * @param $phoneRaw
     *
     * @return SmsSessionToAuth
     * @throws \Exception
     * @throws \app\exceptions\UserNotFoundException
     * @throws \app\exceptions\SmsSessionsToAuthCouldNotSaveException
     */
    public function createSession($phoneRaw)
    {
        $phone = $this->stringsHelper->normolizePhone($phoneRaw);

        //так мы проверяем что пользователь с таким телефоном существует
        $user = $this->userAppManager->getUserByPhone($phone);

        $smsSessionToAuth = $this->smsSessionsToAuthRepository->getSessionActiveByPhone($phone);

        if($smsSessionToAuth === null){
            $smsSessionToAuth = new SmsSessionToAuth([
                'phone' => $phone,
                'pin' => $this->generatePin(),
                'created_at' => date('c'),
                'status' => SmsSessionToAuth::STATUS_ACTIVE,
            ]);

            $this->smsSessionsToAuthRepository->save($smsSessionToAuth);
        }

        $message = sprintf("Пин-код для авторизации : %05d", $smsSessionToAuth->pin);
        $this->smsService->setMessage($message);
        $this->smsService->setPhone($phone);

        $this->smsService->send();

        return $smsSessionToAuth;
    }

    /**
     * @param $phoneRaw
     * @param $pin
     *
     * @return SmsSessionToAuth|null
     * @throws \app\exceptions\smssessionstoauth\SmsSessionsToAuthNotFoundException
     */
    public function getSession($phoneRaw, $pin)
    {
        $phone = $this->stringsHelper->normolizePhone($phoneRaw);

        $smsSessionToAuth = $this->smsSessionsToAuthRepository->getSessionActiveByPhoneAndPin($phone, $pin);
        if ($smsSessionToAuth === null) {
            throw new SmsSessionsToAuthNotFoundException();
        }

        return $smsSessionToAuth;
    }

    /**
     * @param SmsSessionToAuth $smsSessionToAuth
     *
     * @throws \app\exceptions\smssessionstoauth\SmsSessionsToAuthCouldNotSaveException
     */
    public function deleteSession(SmsSessionToAuth $smsSessionToAuth)
    {
        $smsSessionToAuth->status = SmsSessionToAuth::STATUS_DELETED;
        $this->smsSessionsToAuthRepository->save($smsSessionToAuth);
    }

    /**
     * @return string
     */
    private function generatePin()
    {
        return sprintf('%05d', mt_rand(1, 99999));
    }
}