<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 19.09.17
 * Time: 11:50
 */

namespace app\managers;

use app\dto\JwtDto;
use app\dto\TokensDto;
use app\dto\UserAppDto;
use app\models\RefreshToken;
use app\models\UserApp;
use app\repositories\RefreshTokenRepository;
use app\exceptions\RefreshTokenDoNotCreated;
use \Firebase\JWT\JWT;
use app\exceptions\RefreshTokenNotValid;

class JwtManager
{
    const ALGORITHM_ENCRYPTION_ALLOWED = ['HS256',];
    const REFRESH_TOKEN_LIFE_PERIOD = '+ 10 minutes';
    const ACCESS_TOKEN_LIFE_PERIOD = '+ 3 minutes';
    const FORMAT_DATE_FROM_DB = 'Y-m-d H:i:s';

    /* @var string*/
    private $secretKeyForAccessToken;
    /* @var string*/
    private $secretKeyForRefreshToken;
    /** @var RefreshTokenRepository */
    private $refreshTokenRepository;
    private $userAppManager;

    public function __construct($secretKeyForAccessToken, $secretKeyForRefreshToken, RefreshTokenRepository $refreshTokenRepository, UserAppManager $userAppManager)
    {
        $this->secretKeyForAccessToken = $secretKeyForAccessToken;
        $this->secretKeyForRefreshToken = $secretKeyForRefreshToken;
        $this->refreshTokenRepository = $refreshTokenRepository;
        $this->userAppManager =$userAppManager;
    }

    /**
     * @param UserApp $userAppDto
     * @return string
     */
    public function createRefreshToken(UserAppDto $userAppDto)
    {

        $refreshTokenStingValue = $this->checkWhileCreateRefreshToken($userAppDto);
        $refreshToken = new RefreshToken([
            'user_id' => $userAppDto->id,
            'status' => RefreshToken::STATUS_ACTIVE,
            'value' => $refreshTokenStingValue,
            'created_at' => date(self::FORMAT_DATE_FROM_DB),
        ]);
        $this->refreshTokenRepository->save($refreshToken);
        return $refreshTokenStingValue;
    }

    /**
     * @param UserApp $userAppDto
     * @return string
     */
    public function  createAccessToken(TokensDto $tokensDto)
    {
        if ($this->IsTheDateOfTheRefreshTokenNotExpired($tokensDto->refreshToken)) {
            $userId = $this->findUserAppIdByRefreshToken($tokensDto->refreshToken);
            $userApp = $this->userAppManager->getUserAppById($userId);
            $payload = new JwtDto($userApp->id, $userApp->phone_number);
            $accessToken = $this->encode($payload);
            $tokensDtoNew = new TokensDto(null, $accessToken);
            return $tokensDtoNew;
        }
    }

    private function checkWhileCreateRefreshToken(UserAppDto $userAppDto)
    {
        $takt = 0;
        do {
            $refreshTokenStingValue = \Yii::$app->getSecurity()->generateRandomString($userAppDto->phone_number);
            //$refreshTokenStingValue = \Yii::$app->getSecurity()->hashData($userApp->phone_number, $this->secretKeyForAccessToken);
            $takt += 1;
        } while ($this->refreshTokenRepository->getByValue($refreshTokenStingValue) != null || $takt > 100);
        if ($takt > 100) {
            throw RefreshTokenDoNotCreated();
        }
        else{
            return $refreshTokenStingValue;
        }
    }

    /**
     * @param string $currentRefreshToken
     * @return bool
     */
    public function IsTheDateOfTheRefreshTokenNotExpired($currentRefreshToken)
    {
        $refresTokenInDB = $this->refreshTokenRepository->getByValue($currentRefreshToken);
        $createdTime = $refresTokenInDB->createdAt;
        $expirationTime = strtotime($createdTime. self::REFRESH_TOKEN_LIFE_PERIOD);
        $currentDateTime = date(self::FORMAT_DATE_FROM_DB);
        $unixTimeNow = strtotime($currentDateTime );
        if ($expirationTime > $unixTimeNow) {
            return true;
        }
        else {
            throw new RefreshTokenNotValid();
        }
    }

    /**
     * @param string $currentAccessToken
     * @return bool
     *
     * Не нашел реализации в JWT
     *
     */
    public function IsTheDateOfTheAccessTokenNotExpired($currentAccessToken)
    {
        $accessTokenDto = $this->decode($currentAccessToken);
        $createdTime = $accessTokenDto->createdAt;
        $expirationTime = strtotime($createdTime. self::ACCESS_TOKEN_LIFE_PERIOD);
        $currentDateTime = date(self::FORMAT_DATE_FROM_DB);
        $unixTimeNow = strtotime($currentDateTime );
        if ($expirationTime > $unixTimeNow) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param string $currentRefreshToken
     */
    public function deleteRefreshToken($currentRefreshToken)
    {
        $refresTokenInDB = $this->refreshTokenRepository->getByValue($currentRefreshToken);
        $refresTokenInDB->status = RefreshToken::STATUS_DELETED;
        $this->refreshTokenRepository->save($refresTokenInDB);
    }

    /**
     * @param JwtDto $payload
     * @return string
     */
    public function encode(JwtDto $payload)
    {
        return JWT::encode($payload, $this->secretKeyForAccessToken);
    }

    /**
     * @param $jwt
     * @return object
     */
    public function decode($jwt)
    {
        return JWT::decode($jwt, $this->secretKeyForAccessToken, self::ALGORITHM_ENCRYPTION_ALLOWED);
    }

    public function findRefreshTokenInDB($refreshToken)
    {
        $refreshTokenInDB = $this->refreshTokenRepository->getByValue($refreshToken);
        return $refreshTokenInDB;
    }

    public function findUserAppIdByRefreshToken($refreshToken)
    {
        $refreshTokenInDB = $this->findRefreshTokenInDB($refreshToken);
        $userId = $refreshTokenInDB->userId;
        return $userId;
    }

    public function createTwoTokens(UserAppDto $userAppDto)
    {
        $refreshToken = $this->createRefreshToken($userAppDto);
        $accessToken = $this->createAccessToken($userAppDto);
        $tokenDto = new TokensDto($refreshToken, $accessToken);
        return $tokenDto;
    }


}