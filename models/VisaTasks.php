<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visa_tasks".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $type
 * @property string $value
 * @property integer $number_in_anket
 * @property string $rules
 * @property integer $version
 *
 * @property Country $country
 */
class VisaTasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visa_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'type', 'number_in_anket', 'version'], 'integer'],
            [['value', 'rules'], 'string'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'type' => 'Type',
            'value' => 'Value',
            'number_in_anket' => 'Number In Anket',
            'rules' => 'Rules',
            'version' => 'Version',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
