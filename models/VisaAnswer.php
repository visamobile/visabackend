<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visa_answer".
 *
 * @property integer $id
 * @property string $value
 * @property integer $task_id
 */
class VisaAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visa_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['task_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'task_id' => 'Task ID',
        ];
    }
}
