<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visa".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $status
 * @property integer $country_id
 * @property integer $type
 * @property string $created_at
 */
class Visa extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLE = 'disable';
    const STATUS_DELETED = 'deleted';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'country_id', 'type'], 'integer'],
            [['created_at'], 'safe'],
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'country_id' => 'Country ID',
            'type' => 'Type',
            'created_at' => 'Created At',
        ];
    }
}
