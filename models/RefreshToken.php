<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "refresh_token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $status
 * @property string $value
 * @property string $created_at
 */
class RefreshToken extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLE = 'disable';
    const STATUS_DELETED = 'deleted';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'refresh_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['status', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'value' => 'Value',
            'created_at' => 'Created At',
        ];
    }
}
