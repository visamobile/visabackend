<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 21.09.17
 * Time: 18:03
 */


use yii\di\Instance;

$container->setDefinitions(
    [
        \app\managers\JwtManager::class => [
            ['class' => \app\managers\JwtManager::class],
            [
                $params['jwtSecretKey'],
            ],
        ],
        \app\managers\UserAppManager::class => [
            ['class' => \app\managers\UserAppManager::class],
        ],
    ]
);