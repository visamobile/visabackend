<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 16:45
 */

namespace app\controllers;

use app\exceptions\ExceptionsHandler;
use yii\rest\Controller;
use yii\web\Response;
/**
 * Class BaseController
 * @package app\modules\api\controllers
 */
class BaseController extends Controller
{
    /** @var null|array */
    protected $dataRaw = null;
    /**
     * Отлавливает и обрабатывает \Exception
     *
     * @param string $id
     * @param array  $params
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function runAction($id, $params = [])
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $this->dataRaw = json_decode(
            file_get_contents('php://input'),
            true
        );
        /**
         * @var ExceptionsHandler $exceptionsHandler
         */
        $exceptionsHandler = \Yii::$container->get(ExceptionsHandler::class);
        return $exceptionsHandler->execute(
            $id,
            $params,
            function($id, $params) {
                return parent::runAction($id, $params);
            }
        );
    }
}