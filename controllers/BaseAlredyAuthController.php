<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 16:13
 */

namespace app\controllers;

use app\dto\JwtDto;
use app\exceptions\TokenEmptyException;
use app\exceptions\YouNotLogedException;
use app\models\UserApp;
use app\exceptions\ExceptionsHandler;
use app\managers\JwtManager;
use yii\rest\Controller;
//use yii\web\Controller;
use yii\web\Response;
/**
 * Class BaseAlredyAuthController
 * @package app\modules\api\controllers
 */
class BaseAlredyAuthController extends Controller
{
    /** @var null|array */
    protected $dataRaw = null;
    /** @var User */
    protected $user = null;
    /** @var JwtDataDto */
    private $userJwtData = null;
    /**
     * Отлавливает и обрабатывает \Exception
     *
     * @param string $id
     * @param array  $params
     *
     * @return mixed
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function runAction($id, $params = [])
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $this->dataRaw = json_decode(file_get_contents('php://input'), true);
        /**
         * @var ExceptionsHandler $exceptionsHandler
         */
        $exceptionsHandler = \Yii::$container->get(ExceptionsHandler::class);
        $exceptionsHandler->executeFn(
            function() {
                $this->setUser();
            }
        );
        return $exceptionsHandler->execute(
            $id,
            $params,
            function($id, $params) {
                return parent::runAction($id, $params);
            }
        );
    }
    public function setUser()
    {
        $headerAuthorization = \Yii::$app->request->headers->get('Authorization');
        if ($headerAuthorization === null) {
            throw new YouNotLogedException();
        }
        $strBearer = 'Bearer';
        $token     = trim(str_replace($strBearer, "", $headerAuthorization));
        if ($token === null || $token === '') {
            throw new TokenEmptyException();
        }
        /** @var JwtManager $jwtManager */
        $jwtManager        = \Yii::$container->get(JwtManager::class);
        $this->userJwtData = $jwtManager->decode($token);
    }
    /**
     * @return User
     */
    protected function getUserIdentity()
    {
        if ($this->user === null) {
            $this->user = UserApp::findOne(['id' => $this->userJwtData->userId]);
        }
        return $this->user;
    }
    /**
     * Разница от getUserIdentity в том что нет запроса в базу
     *
     * @return JwtDataDto
     */
    protected function getUserJwtData()
    {
        return $this->userJwtData;
    }
}