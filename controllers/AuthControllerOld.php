<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 20.09.17
 * Time: 16:27
 */

namespace app\controllers;

use app\dto\UserAppDto;
use app\managers\JwtManager;
use app\managers\UserAppManager;
use app\models\UserApp;
use yii\web\Controller;

class AuthControllerOld extends Controller
{
    /** @var UserAppManager */
    private $userAppManager;
    /** @var JwtManager */
    private $jwtManager;

    /**
     * AuthControllerOld constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param UserAppManager $userAppManager
     * @param JwtManager $jwtManager
     * @param array $config
     */
    public function __construct($id, $module, UserAppManager $userAppManager, JwtManager $jwtManager, array $config = [])
    {
        $this->userAppManager = $userAppManager;
        $this->jwtManager = $jwtManager;
        parent::__construct($id, $module, $config);
    }

    /**
     * @param $phoneNumber
     * @param $country
     * @param $language
     * @param null $email
     * @return bool
     * @throws \Exception
     */
    public function actionRegisterUser($phoneNumber, $country, $language, $email = null)
    {
        $this->userAppManager->IsExistUserForThisPhoneNumber($phoneNumber);
        $userDto = new UserAppDto($phoneNumber, $country, $language, $email);
        $userApp = $this->userAppManager->createNewUserApp($userDto);
        $refreshToken = $this->createRefrehsToken($userApp);
        $accessToken = $this->createAccessToken($userApp);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'refreshToken' => $refreshToken,
                'accessToken' => $accessToken,
            ];
    }

    /**
     * @param UserApp $userApp
     * @return string
     */
    private function createRefrehsToken(UserApp $userApp)
    {
        return $this->jwtManager->createRefreshToken($userApp);
    }

    private function createAccessToken(UserApp $userApp)
    {
        return $this->jwtManager->createAccessToken($userApp);
    }

    /**
     * @param $accessToken
     * @throws \Exception
     * хочу сделать метод который возвращает статус логирования после того как пользователь зарегестрировался. Он жмет вход
     * на сервер отправляется access токен, он проверяется, если токен валидный то сервер отвечает  810 app попадает на экран главного меню.
     * Если код 809 то app делает запрос на получение нового access  token.
     */
    public function actionLogin($accessToken)
    {
        $access = '';
        if ($this->jwtManager->IsTheDateOfTheAccessTokenNotExpired($accessToken)) {
             $access = 'granted';
            throw new \Exception('Access token valid.', 810);
        }
        else {
             $access = 'denied';
            throw new \Exception('Access token not valid.', 809);

        }
    }

    /**
     * @param $refreshToken
     * @return string
     */
    public function actionReturnRefreshToken($refreshToken)
    {
        $userId = $this->jwtManager->findUserAppIdByRefreshToken($refreshToken);
        $userApp = $this->userAppManager->getUserAppById($userId);
        $this->jwtManager->deleteRefreshToken($refreshToken);
        $refreshToken = $this->createRefrehsToken($userApp);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'refreshToken' => $refreshToken,
        ];
    }

    /**
     * @param $refreshToken
     * @return string
     * @throws \Exception
     */
    public function actionReturnAccessToken($refreshToken)
    {
        if ($this->jwtManager->IsTheDateOfTheRefreshTokenNotExpired($refreshToken)) {
            $userId = $this->jwtManager->findUserAppIdByRefreshToken($refreshToken);
            $userApp = $this->userAppManager->getUserAppById($userId);

            $accessToken = $this->createAccessToken($userApp);
            return [
                'accessToken' => $accessToken,
            ];
        }
        else {
            throw new \Exception('Refresh token not valid.', 811);
        }
    }
}