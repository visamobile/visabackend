<?php
/**
 * Created by PhpStorm.
 * User: revnrevn
 * Date: 02.10.17
 * Time: 17:00
 */

namespace app\controllers;

use app\dto\TokensDto;
use app\dto\UserAppDto;
use app\exceptions\UserNotFoundException;
use app\managers\SmsSessionsToAuthManager;
use app\managers\UserAppManager;
use app\dto\AuthorizationDto;
use app\dto\ResponseGetTokenDto;
use app\dto\SendSmsDto;
use app\dto\BaseResponseDto;
use app\managers\JwtManager;
use exceptions\RefreshTokenNotValid;

/**
 * Контроллер Авторизации
 *
 * Class AuthController
 * @package app\modules\api\controllers
 */
class AuthController extends BaseController
{
    /** @var SmsSessionsToAuthManager */
    private $smsSessionsToAuthManager;
    /** @var UserManager */
    private $userAppManager;
    /** @var JwtManager */
    private $jwtManager;

    /**
     * AuthController constructor.
     *
     * @param string                   $id
     * @param \yii\base\Module         $module
     * @param SmsSessionsToAuthManager $smsSessionsToAuthManager
     * @param JwtManager               $jwtManager
     * @param UserManager              $userAppManager
     * @param array                    $config
     */
    public function __construct(
        $id,
        $module,
        SmsSessionsToAuthManager $smsSessionsToAuthManager,
        JwtManager $jwtManager,
        UserAppManager $userAppManager,
        array $config = []
    ) {
        $this->smsSessionsToAuthManager = $smsSessionsToAuthManager;
        $this->userAppManager              = $userAppManager;
        $this->jwtManager               = $jwtManager;
        parent::__construct($id, $module, $config);
    }

    /**
     * Отправка смс для авториазации
     *
     * @SWG\Post(path="/auth/send-sms",
     *   tags={"auth"},
     *   summary="Запрос отправки смс для авториазации",
     *   description="Запрос отправки смс для авториазации",
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Объект приходит в json формате",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/SendSmsDto")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Успешное выполнение",
     *     @SWG\Schema(ref="#/definitions/BaseResponseDto")
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Ошибка выполнения",
     *     @SWG\Schema(ref="#/definitions/ResponseErrorDto")
     *   )
     * )
     *
     * @return BaseResponseDto
     *
     * @throws \Exception
     * @throws \app\exceptions\UserNotFoundException
     * @throws \app\exceptions\SmsSessionsToAuthCouldNotSaveException
     */
    public function actionSendSms()
    {
        $sendSmsDto = SendSmsDto::loadFromArray($this->dataRaw);
        $this->smsSessionsToAuthManager->createSession($sendSmsDto->phone);

        return BaseResponseDto::create();
    }

    /**
     * Получения токена
     *
     * @SWG\Post(path="/auth/get-token",
     *   tags={"auth"},
     *   summary="получения токена",
     *   description="получения токена",
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Объект приходит в json формате",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/AuthorizationDto")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Успешное выполнение",
     *     @SWG\Schema(ref="#/definitions/ResponseGetTokenDto")
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Ошибка выполнения",
     *     @SWG\Schema(ref="#/definitions/ResponseErrorDto")
     *   )
     * )
     *
     * @return ResponseGetTokenDto
     *
     * @return array
     * @throws \app\exceptions\SmsSessionsToAuthCouldNotSaveException
     * @throws \app\exceptions\UserNotFoundException
     * @throws \app\exceptions\SmsSessionsToAuthNotFoundException
     */
    public function actionGetRefreshToken()
    {
        $authorizationDto  = AuthorizationDto::loadFromArray($this->dataRaw);
        $smsSessionsToAuth = $this->smsSessionsToAuthManager->getSession($authorizationDto->phone,
            $authorizationDto->pin);
        $user              = $this->userAppManager->getUserByPhone($smsSessionsToAuth->phone);

        if ($smsSessionsToAuth->phone == '79653289536' && $smsSessionsToAuth->pin == '37042') {
            // костыль для прохождения модерации App Store
        } else {
            $this->smsSessionsToAuthManager->deleteSession($smsSessionsToAuth);
        }

        return ResponseGetTokenDto::create($this->jwtManager->createRefreshToken($user));
    }

    public function actionRegisterUser()
    {
        $userAppDto = UserAppDto::loadFromArray($this->dataRaw);
        $userApp = $this->userAppManager->createNewUserApp($userAppDto);
        return $this->jwtManager->createTwoTokens($userApp);
    }

    public function actionGetAccessToken()
    {
        $tokens = TokensDto::loadFromArray($this->dataRaw);
        return $this->jwtManager->createAccessToken($tokens);
    }

}